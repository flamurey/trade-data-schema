// automatically generated by the FlatBuffers compiler, do not modify

package market

import (
	flatbuffers "github.com/google/flatbuffers/go"
)

type Client struct {
	_tab flatbuffers.Table
}

func GetRootAsClient(buf []byte, offset flatbuffers.UOffsetT) *Client {
	n := flatbuffers.GetUOffsetT(buf[offset:])
	x := &Client{}
	x.Init(buf, n+offset)
	return x
}

func (rcv *Client) Init(buf []byte, i flatbuffers.UOffsetT) {
	rcv._tab.Bytes = buf
	rcv._tab.Pos = i
}

func (rcv *Client) Id() []byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(4))
	if o != 0 {
		return rcv._tab.ByteVector(o + rcv._tab.Pos)
	}
	return nil
}

func (rcv *Client) Type() byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(6))
	if o != 0 {
		return rcv._tab.GetByte(o + rcv._tab.Pos)
	}
	return 0
}

func (rcv *Client) MutateType(n byte) bool {
	return rcv._tab.MutateByteSlot(6, n)
}

func (rcv *Client) Currency() uint16 {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(8))
	if o != 0 {
		return rcv._tab.GetUint16(o + rcv._tab.Pos)
	}
	return 0
}

func (rcv *Client) MutateCurrency(n uint16) bool {
	return rcv._tab.MutateUint16Slot(8, n)
}

func (rcv *Client) Markets(j int) uint16 {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(10))
	if o != 0 {
		a := rcv._tab.Vector(o)
		return rcv._tab.GetUint16(a + flatbuffers.UOffsetT(j*2))
	}
	return 0
}

func (rcv *Client) MarketsLength() int {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(10))
	if o != 0 {
		return rcv._tab.VectorLen(o)
	}
	return 0
}

func ClientStart(builder *flatbuffers.Builder) {
	builder.StartObject(4)
}
func ClientAddId(builder *flatbuffers.Builder, id flatbuffers.UOffsetT) {
	builder.PrependUOffsetTSlot(0, flatbuffers.UOffsetT(id), 0)
}
func ClientAddType(builder *flatbuffers.Builder, type byte) {
	builder.PrependByteSlot(1, type, 0)
}
func ClientAddCurrency(builder *flatbuffers.Builder, currency uint16) {
	builder.PrependUint16Slot(2, currency, 0)
}
func ClientAddMarkets(builder *flatbuffers.Builder, markets flatbuffers.UOffsetT) {
	builder.PrependUOffsetTSlot(3, flatbuffers.UOffsetT(markets), 0)
}
func ClientStartMarketsVector(builder *flatbuffers.Builder, numElems int) flatbuffers.UOffsetT {
	return builder.StartVector(2, numElems, 2)
}
func ClientEnd(builder *flatbuffers.Builder) flatbuffers.UOffsetT {
	return builder.EndObject()
}
