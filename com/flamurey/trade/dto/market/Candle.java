// automatically generated by the FlatBuffers compiler, do not modify

package com.flamurey.trade.dto.market;

import java.nio.*;
import java.lang.*;
import java.util.*;
import com.google.flatbuffers.*;

@SuppressWarnings("unused")
public final class Candle extends Table {
  public static Candle getRootAsCandle(ByteBuffer _bb) { return getRootAsCandle(_bb, new Candle()); }
  public static Candle getRootAsCandle(ByteBuffer _bb, Candle obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__assign(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public void __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; }
  public Candle __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public Bar bar() { return bar(new Bar()); }
  public Bar bar(Bar obj) { int o = __offset(4); return o != 0 ? obj.__assign(o + bb_pos, bb) : null; }
  public double openInterest() { int o = __offset(6); return o != 0 ? bb.getDouble(o + bb_pos) : 0.0; }

  public static void startCandle(FlatBufferBuilder builder) { builder.startObject(2); }
  public static void addBar(FlatBufferBuilder builder, int barOffset) { builder.addStruct(0, barOffset, 0); }
  public static void addOpenInterest(FlatBufferBuilder builder, double openInterest) { builder.addDouble(1, openInterest, 0.0); }
  public static int endCandle(FlatBufferBuilder builder) {
    int o = builder.endObject();
    builder.required(o, 4);  // bar
    return o;
  }
}

