// automatically generated by the FlatBuffers compiler, do not modify

package com.flamurey.trade.dto.market;

public final class ClientType {
  private ClientType() { }
  public static final byte NULL = 0;
  public static final byte SPOT = 1;
  public static final byte LEVERAGE = 2;
  public static final byte MARGIN_LEVEL = 3;

  public static final String[] names = { "NULL", "SPOT", "LEVERAGE", "MARGIN_LEVEL", };

  public static String name(int e) { return names[e]; }
}

