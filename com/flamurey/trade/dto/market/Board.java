// automatically generated by the FlatBuffers compiler, do not modify

package com.flamurey.trade.dto.market;

import java.nio.*;
import java.lang.*;
import java.util.*;
import com.google.flatbuffers.*;

@SuppressWarnings("unused")
public final class Board extends Table {
  public static Board getRootAsBoard(ByteBuffer _bb) { return getRootAsBoard(_bb, new Board()); }
  public static Board getRootAsBoard(ByteBuffer _bb, Board obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__assign(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public void __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; }
  public Board __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public String code() { int o = __offset(4); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer codeAsByteBuffer() { return __vector_as_bytebuffer(4, 1); }
  public String name() { int o = __offset(6); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer nameAsByteBuffer() { return __vector_as_bytebuffer(6, 1); }
  public int market() { int o = __offset(8); return o != 0 ? bb.getShort(o + bb_pos) & 0xFFFF : 0; }
  public int type() { int o = __offset(10); return o != 0 ? bb.get(o + bb_pos) & 0xFF : 0; }

  public static int createBoard(FlatBufferBuilder builder,
      int codeOffset,
      int nameOffset,
      int market,
      int type) {
    builder.startObject(4);
    Board.addName(builder, nameOffset);
    Board.addCode(builder, codeOffset);
    Board.addMarket(builder, market);
    Board.addType(builder, type);
    return Board.endBoard(builder);
  }

  public static void startBoard(FlatBufferBuilder builder) { builder.startObject(4); }
  public static void addCode(FlatBufferBuilder builder, int codeOffset) { builder.addOffset(0, codeOffset, 0); }
  public static void addName(FlatBufferBuilder builder, int nameOffset) { builder.addOffset(1, nameOffset, 0); }
  public static void addMarket(FlatBufferBuilder builder, int market) { builder.addShort(2, (short)market, 0); }
  public static void addType(FlatBufferBuilder builder, int type) { builder.addByte(3, (byte)type, 0); }
  public static int endBoard(FlatBufferBuilder builder) {
    int o = builder.endObject();
    builder.required(o, 4);  // code
    builder.required(o, 6);  // name
    return o;
  }
}

