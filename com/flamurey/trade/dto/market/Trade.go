// automatically generated by the FlatBuffers compiler, do not modify

package market

import (
	flatbuffers "github.com/google/flatbuffers/go"
)

type Trade struct {
	_tab flatbuffers.Table
}

func GetRootAsTrade(buf []byte, offset flatbuffers.UOffsetT) *Trade {
	n := flatbuffers.GetUOffsetT(buf[offset:])
	x := &Trade{}
	x.Init(buf, n+offset)
	return x
}

func (rcv *Trade) Init(buf []byte, i flatbuffers.UOffsetT) {
	rcv._tab.Bytes = buf
	rcv._tab.Pos = i
}

func (rcv *Trade) OrderNo() int64 {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(4))
	if o != 0 {
		return rcv._tab.GetInt64(o + rcv._tab.Pos)
	}
	return 0
}

func (rcv *Trade) MutateOrderNo(n int64) bool {
	return rcv._tab.MutateInt64Slot(4, n)
}

func (rcv *Trade) SecCode() []byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(6))
	if o != 0 {
		return rcv._tab.ByteVector(o + rcv._tab.Pos)
	}
	return nil
}

func (rcv *Trade) Board() []byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(8))
	if o != 0 {
		return rcv._tab.ByteVector(o + rcv._tab.Pos)
	}
	return nil
}

func (rcv *Trade) OrderDirection() byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(10))
	if o != 0 {
		return rcv._tab.GetByte(o + rcv._tab.Pos)
	}
	return 0
}

func (rcv *Trade) MutateOrderDirection(n byte) bool {
	return rcv._tab.MutateByteSlot(10, n)
}

func (rcv *Trade) Time() int64 {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(12))
	if o != 0 {
		return rcv._tab.GetInt64(o + rcv._tab.Pos)
	}
	return 0
}

func (rcv *Trade) MutateTime(n int64) bool {
	return rcv._tab.MutateInt64Slot(12, n)
}

func (rcv *Trade) Nano() int32 {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(14))
	if o != 0 {
		return rcv._tab.GetInt32(o + rcv._tab.Pos)
	}
	return 0
}

func (rcv *Trade) MutateNano(n int32) bool {
	return rcv._tab.MutateInt32Slot(14, n)
}

func (rcv *Trade) Price() float64 {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(16))
	if o != 0 {
		return rcv._tab.GetFloat64(o + rcv._tab.Pos)
	}
	return 0.0
}

func (rcv *Trade) MutatePrice(n float64) bool {
	return rcv._tab.MutateFloat64Slot(16, n)
}

func (rcv *Trade) Quantity() float64 {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(18))
	if o != 0 {
		return rcv._tab.GetFloat64(o + rcv._tab.Pos)
	}
	return 0.0
}

func (rcv *Trade) MutateQuantity(n float64) bool {
	return rcv._tab.MutateFloat64Slot(18, n)
}

func (rcv *Trade) OpenInterest() float64 {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(20))
	if o != 0 {
		return rcv._tab.GetFloat64(o + rcv._tab.Pos)
	}
	return 0.0
}

func (rcv *Trade) MutateOpenInterest(n float64) bool {
	return rcv._tab.MutateFloat64Slot(20, n)
}

func TradeStart(builder *flatbuffers.Builder) {
	builder.StartObject(9)
}
func TradeAddOrderNo(builder *flatbuffers.Builder, orderNo int64) {
	builder.PrependInt64Slot(0, orderNo, 0)
}
func TradeAddSecCode(builder *flatbuffers.Builder, secCode flatbuffers.UOffsetT) {
	builder.PrependUOffsetTSlot(1, flatbuffers.UOffsetT(secCode), 0)
}
func TradeAddBoard(builder *flatbuffers.Builder, board flatbuffers.UOffsetT) {
	builder.PrependUOffsetTSlot(2, flatbuffers.UOffsetT(board), 0)
}
func TradeAddOrderDirection(builder *flatbuffers.Builder, orderDirection byte) {
	builder.PrependByteSlot(3, orderDirection, 0)
}
func TradeAddTime(builder *flatbuffers.Builder, time int64) {
	builder.PrependInt64Slot(4, time, 0)
}
func TradeAddNano(builder *flatbuffers.Builder, nano int32) {
	builder.PrependInt32Slot(5, nano, 0)
}
func TradeAddPrice(builder *flatbuffers.Builder, price float64) {
	builder.PrependFloat64Slot(6, price, 0.0)
}
func TradeAddQuantity(builder *flatbuffers.Builder, quantity float64) {
	builder.PrependFloat64Slot(7, quantity, 0.0)
}
func TradeAddOpenInterest(builder *flatbuffers.Builder, openInterest float64) {
	builder.PrependFloat64Slot(8, openInterest, 0.0)
}
func TradeEnd(builder *flatbuffers.Builder) flatbuffers.UOffsetT {
	return builder.EndObject()
}
